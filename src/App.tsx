import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <div className="alert alert-primary w-25" role="alert">
      A simple primary alert—check it out!
    </div>
  );
}

export default App;
