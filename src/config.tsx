const config: any = {
  mode: process.env.REACT_APP_MODE,
  facadeURL: process.env.REACT_APP_FACADE,
  awsStartProcess: process.env.REACT_APP_AWS_START_PROCESS,
  applicationId: process.env.REACT_APP_APPLICATION_ID,
  baseURL: process.env.REACT_APP_BASE_URL,
  prefix: process.env.REACT_APP_PREFIX,
  redirectURL: process.env.REACT_APP_REDIRECT,
  definitionKey: process.env.REACT_APP_PROCESS_DEF_KEY,
};

export default config;
