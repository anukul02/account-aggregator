const { createProxyMiddleware } = require("http-proxy-middleware");

const turiyaDev = false; // set this to false to point to turiya uat

const env = turiyaDev ? "dev" : "uat";
const baseUrl = turiyaDev
  ? "https://turiya.staging.protium.co.in"
  : "https://turiya.gsft.protium.net.in";

module.exports = (app) => {
  // this section below will expose /env and /baseUrl
  // through webpack dev server when developing locally
  // the equivalent setting when running on a hosted environment (uat/prod) using nginx is in conf.template.nginx

  app.get("/env", (req, res) => res.send(env));
  app.get("/base", (req, res) => res.send(baseUrl));

  // this is the section that proxies calls to /auth to keycloak during development
  // for an equivalent config for nginx server, check conf.template.nginx
  addProxyMiddleware(app, "/auth", baseUrl);
};

const addProxyMiddleware = (app, path, target, basePathToRemove) => {
  app.use(
    path,
    createProxyMiddleware({
      target: target,
      secure: false,
      logLevel: "info",
      changeOrigin: true,
      cookieDomainRewrite: "localhost",
      pathRewrite: basePathToRemove
        ? (fullPath) => fullPath.replace(basePathToRemove, "")
        : undefined,
    })
  );
};
