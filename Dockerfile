FROM nginx:stable-alpine as server
EXPOSE 80
COPY build /usr/share/nginx/html
COPY health /usr/share/nginx/html/health
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
ENTRYPOINT ["nginx", "-g", "daemon off;"]